extends Control

var tab_counter := 1
@onready var tab_container := %TabContainer
signal reset_settings
signal new_canvasitem_shader(s)
signal new_spatial_shader(s)
signal new_resolution(r)
signal new_fft_size(s)
signal new_band_config(band)
signal new_cc_config(c)
signal new_pad_config(c)
signal new_key_config(c)
signal new_pressure_config(c)

func _ready():
	tab_container.set_tab_title(0, "  0  ")
	var tabs = Saver.get_value("tabs")
	if(tabs == null):
		return
	if(len(tabs) == 0):
		return
	for old_child in tab_container.get_children():
		old_child.queue_free()
	for tab in tabs:
		new_tab(tab["text"], tab["title"])
	compile_shader()


func _unhandled_input(event):
	if(event.is_action_pressed("compile")):
		get_viewport().set_input_as_handled()
		compile_shader()
	elif(event.is_action_pressed("new_canvasitem")):
		get_viewport().set_input_as_handled()
		new_tab(get_file_as_text("res://shader/default_canvasitem.txt"))
	elif(event.is_action_pressed("new_spatial")):
		get_viewport().set_input_as_handled()
		new_tab(get_file_as_text("res://shader/default_spatial.txt"))
	elif(event.is_action_pressed("change_tab")):
		get_viewport().set_input_as_handled()
		change_tab(-1 if Input.is_action_pressed("shift") else 1)
	elif(event.is_action_pressed("close_tab")):
		get_viewport().set_input_as_handled()
		if(tab_container.get_child_count() == 1):
			return
		tab_container.get_child(tab_container.current_tab).queue_free()
	elif(event.is_action_pressed("save")):
		get_viewport().set_input_as_handled()
		save_state()


func _exit_tree():
	save_state()


func save_state():
	var data := []
	var i := 0
	for editor in tab_container.get_children():
		data.append({
			"title": tab_container.get_tab_title(i),
			"text": editor.text
		})
		i += 1
	Saver.set_value("tabs", data)


func get_file_as_text(path: String):
	var f := FileAccess.open(path, FileAccess.READ)
	if(not is_instance_valid(f)):
		return ""
	var text := f.get_as_text()
	f.close()
	return text


const resolution_tag = "$resolution("
const fft_tag = "$fft_size("
const band_tag = "$band("
const pad_tag = "$pad("
const key_tag = "$key("
const pressure_tag = "$pressure("
const cc_tag = "$cc("
func compile_shader():
	reset_settings.emit()
	
	var shader := Shader.new()
	var text: String = tab_container.get_child(tab_container.current_tab).text
	
	# resolution configuration
	var resolution_config := parse_config(text, resolution_tag)
	text = resolution_config.new_text
	var resolution := float(resolution_config.params_arr[0] if len(resolution_config.params_arr) > 0 else "1.0")
	if(resolution > 1.0 or resolution <= 0.0):
		resolution = 1.0
	new_resolution.emit(resolution)
	
	# fft size configuration
	var fft_config := parse_config(text, fft_tag)
	text = fft_config.new_text
	var fft_size := int(fft_config.params_arr[0] if len(fft_config.params_arr) > 0 else "2")
	if(fft_size < 0 or fft_size > 4):
		fft_size = 2
	new_fft_size.emit(fft_size)
	
	# parse hint macros
	for tag in [pad_tag, key_tag, pressure_tag, cc_tag, band_tag]:
		var hint_macro := parse_hint_macro(text, tag)
		while(hint_macro.index != -1):
			text = hint_macro.new_text
			var params := hint_macro.params
			match tag:
				pad_tag:
					var c := MidiPadConfig.new()
					c.uniform_name = hint_macro.uniform_name
					if(params.has('channel')):
						c.channel = int(params['channel'])-1
					if(params.has('a')):
						c.adsr.a = float(params['a'])
					if(params.has('d')):
						c.adsr.d = float(params['d'])
					if(params.has('s')):
						c.adsr.s = float(params['s'])
					if(params.has('r')):
						c.adsr.r = float(params['r'])
					if(params.has('note')):
						c.note = int(params['note'])
					if(params.has('velocity')):
						c.velocity_contribution = float(params['velocity'])
					if(params.has('min')):
						c.min = float(params['min'])
					if(params.has('max')):
						c.max = float(params['max'])
					new_pad_config.emit(c)
				key_tag:
					var c := MidiKeyConfig.new()
					c.uniform_name = hint_macro.uniform_name
					if(params.has('channel')):
						c.channel = int(params['channel'])-1
					if(params.has('min_note')):
						c.min_note = int(params['min_note'])
					if(params.has('max_note')):
						c.max_note = int(params['max_note'])
					new_key_config.emit(c)
				pressure_tag:
					var c := MidiPressureConfig.new()
					c.uniform_name = hint_macro.uniform_name
					if(params.has('channel')):
						c.channel = int(params['channel'])-1
					if(params.has('min')):
						c.min = float(params['min'])
					if(params.has('max')):
						c.max = float(params['max'])
					new_pressure_config.emit(c)
				cc_tag:
					var c := MidiCcConfig.new()
					c.uniform_name = hint_macro.uniform_name
					if(params.has('cc')):
						c.cc = int(params['cc'])
					if(params.has('min')):
						c.min = float(params['min'])
					if(params.has('max')):
						c.max = float(params['max'])
					new_cc_config.emit(c)
				band_tag:
					var c := FrequencyBand.new()
					c.uniform_name = hint_macro.uniform_name
					if(params.has('min_freq')):
						c.min_freq = float(params['min_freq'])
					if(params.has('max_freq')):
						c.max_freq = float(params['max_freq'])
					if(params.has('min')):
						c.min_freq = float(params['min'])
					if(params.has('max')):
						c.max_freq = float(params['max'])
					c.bypass_damper = true
					if(params.has('f')):
						c.f = float(params['f'])
						c.bypass_damper = false
					if(params.has('r')):
						c.r = float(params['r'])
						c.bypass_damper = false
					if(params.has('z')):
						c.z = float(params['z'])
						c.bypass_damper = false
					new_band_config.emit(c)
			hint_macro = parse_hint_macro(text, tag, hint_macro.index+1)
	shader.code = text
	var shader_type := get_shader_type(shader.code)
	if(shader_type == "spatial"):
		new_spatial_shader.emit(shader)
	else:
		new_canvasitem_shader.emit(shader)


func parse_hint_macro(text: String, tag: String, search_start: int = 0) -> HintMacroResult:
	var tag_start = text.find(tag, search_start)
	if(tag_start == -1):
		return HintMacroResult.new({}, text, "", tag_start)
	var end = text.find(")", tag_start)
	var values: PackedStringArray = text.substr(tag_start+len(tag),
			end-tag_start-len(tag))\
			.replace(" ", "").replace("\n", "").split(",")
	var params := {}
	for value in values:
		var split = value.split("=")
		if(len(split) != 2):
			continue
		params[split[0]] = split[1]
	var colon := text.rfind(":", tag_start)
	var uniform := text.rfind("uniform ", colon)
	var second_space := text.find(" ", uniform+len("uniform "))
	var uniform_name_idx = second_space
	while(text[uniform_name_idx] == ' '):
		uniform_name_idx += 1
	var uniform_name := text.substr(uniform_name_idx, colon-uniform_name_idx)
	text = text.substr(0, colon)+text.substr(end+1)
	return HintMacroResult.new(params, text, uniform_name, tag_start)


class HintMacroResult:
	var params: Dictionary
	var new_text: String
	var uniform_name: String
	var index: int
	func _init(p: Dictionary, n: String, u: String, i: int):
		self.params = p
		self.new_text = n
		self.uniform_name = u
		self.index = i


func parse_config(text: String, tag: String,
		search_start: int = 0, use_dictionary: bool = false) -> MacroResult:
	var start = text.find(tag, search_start)
	if(start == -1):
		return MacroResult.new([], {}, text, -1)
	var end = text.find(")", start)
	var values: PackedStringArray = text.substr(start+len(tag),
			end-start-len(tag))\
			.replace(" ", "").replace("\n", "").split(",")
	text = string_slice(text, 0, start)+string_slice(text, end+1, -1)
	if(not use_dictionary):
		return MacroResult.new(values, {}, text, start)
	var params := {}
	for value in values:
		var split = value.split("=")
		if(len(split) != 2):
			continue
		params[split[0]] = split[1]
	return MacroResult.new([], params, text, start)


class MacroResult:
	var params_dict: Dictionary
	var params_arr: Array
	var new_text: String
	var index: int
	func _init(p_a: Array, p_d: Dictionary, n: String, i: int):
		self.params_arr = p_a
		self.params_dict = p_d
		self.new_text = n
		self.index = i


func get_one_line_config(text: String, tag: String, search_start: int = 0) -> SearchResult:
	var start = text.find(tag, search_start)
	if(start == -1):
		return SearchResult.new("", -1)
	var end = text.find("\n", start)
	var length = end-(start+len(tag))
	if(end == -1):
		length = -1
	var config = text.substr(start+len(tag),
			length)
	return SearchResult.new(config, start)


class SearchResult:
	var text: String
	var index: int
	func _init(t: String, i: int):
		self.text = t
		self.index =  i


func new_tab(content: String, title: String = ""):
	var new := preload("res://text_edit.tscn").instantiate()
	new.text = content
	tab_container.add_child(new)
	if(title == ""):
		title = "  %d  "%tab_counter
	tab_container.set_tab_title(tab_container.get_child_count()-1, title)
	tab_counter += 1
	tab_container.move_child(new, tab_container.current_tab+1)
	change_tab(1)


func change_tab(offset: int):
	var tab_count := tab_container.get_child_count()
	if(tab_count == 0):
		return
	tab_container.current_tab = \
			posmod(tab_container.current_tab+offset, \
			tab_count)
#	await get_tree().process_frame
#	var tab: TextEdit = tab_container.get_child(tab_container.current_tab)
#	tab.syntax_highlighter.clear_highlighting_cache()


func get_shader_type(shader_code: String) -> String:
	var stripped := shader_code.lstrip(" \n\t").trim_prefix("shader_type").lstrip(" \n\t")
	var semicolon := stripped.find(";")
	if(semicolon == -1):
		return "unknown"
	return stripped.substr(0, semicolon)


# [start, end)
func string_slice(str:String, start:int, end:int):
	if(end < 0):
		return str.substr(start)
	return str.substr(start, end-start)
