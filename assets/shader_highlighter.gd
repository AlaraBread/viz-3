@tool
extends TextEdit

const keyword_color = Color(1, 0.46274510025978, 0.46666666865349)
const builtin_color = Color(1, 0.4471310377121, 0.88288938999176)
const comment_color = Color(0.60799998044968, 0.57760000228882, 0.75999999046326)
const preprocessor_color = Color(0.97600001096725, 1, 0.51999998092651)

func _ready():
	syntax_highlighter = preload("res://assets/shader_highlighter.tres").duplicate()
	syntax_highlighter.keyword_colors = keyword_colors
	syntax_highlighter.color_regions = region_colors
	ResourceSaver.save(syntax_highlighter, "res://assets/shader_highlighter.tres")

# we are defining these here cause text is easier
# than using untyped dictionaries in the inspector.

var region_colors := {
	"//": comment_color,
	"/* */": comment_color,
	"#": preprocessor_color,
}

var keyword_colors := {
# gdscript
	"func": keyword_color,
	"pass": keyword_color,

# color hints
	"r": Color(1, 0, 0),
	"g": Color(0, 1, 0),
	"b": Color(0, 0, 1),
	"rg": Color(1, 1, 0),
	"rb": Color(1, 0, 1),
	"bg": Color(0, 1, 1),
	"br": Color(1, 0, 1),
	"gr": Color(1, 1, 0),
	"gb": Color(0, 1, 1),
	"rgb": Color(1, 1, 1),
	"rbg": Color(1, 1, 1),
	"grb": Color(1, 1, 1),
	"gbr": Color(1, 1, 1),
	"brg": Color(1, 1, 1),
	"bgr": Color(1, 1, 1),

# godot specific
# im not including all of these, just ones relevant to fragment shaders
	"TIME": builtin_color,

	"PI": builtin_color,
	"TAU": builtin_color,
	"E": builtin_color,

	"FRAGCOORD": builtin_color,
	"SCREEN_PIXEL_SIZE": builtin_color,
	"POINT_COORD": builtin_color,
	"TEXTURE": builtin_color,
	"TEXTURE_PIXEL_SIZE": builtin_color,
	"AT_LIGHT_PASS": builtin_color,
	"SPECULAR_SHININESS_TEXTURE": builtin_color,
	"SPECULAR_SHININESS": builtin_color,

	"UV": builtin_color,
	"SCREEN_UV": builtin_color,
	"SCREEN_TEXTURE": builtin_color,

	"NORMAL": builtin_color,
	"NORMAL_TEXTURE": builtin_color,
	"NORMAL_MAP": builtin_color,
	"NORMAL_MAP_DEPTH": builtin_color,

	"VERTEX": builtin_color,
	"SHADOW_VERTEX": builtin_color,
	"LIGHT_VERTEX": builtin_color,

	"COLOR": builtin_color,
	"ALBEDO": builtin_color,

	"shader_type": keyword_color,
	"canvas_item": builtin_color,
	"spatial": builtin_color,
	
	"render_mode": keyword_color,

# glsl keywords
	"attribute": keyword_color,
	"const": keyword_color,
	"uniform": keyword_color,
	"varying": keyword_color,

	"layout": keyword_color,

	"centroid": keyword_color,
	"flat": keyword_color,
	"smooth": keyword_color,
	"noperspective": keyword_color,

	"break": keyword_color,
	"continue": keyword_color,
	"do": keyword_color,
	"for": keyword_color,
	"while": keyword_color,
	"switch": keyword_color,
	"case": keyword_color,
	"default": keyword_color,

	"if": keyword_color,
	"else": keyword_color,

	"in": keyword_color,
	"out": keyword_color,
	"inout": keyword_color,

	"float": keyword_color,
	"int": keyword_color,
	"void": keyword_color,
	"bool": keyword_color,
	"true": keyword_color,
	"false": keyword_color,

	"invariant": keyword_color,

	"discard": keyword_color,
	"return": keyword_color,

	"mat2": keyword_color,
	"mat3": keyword_color,
	"mat4": keyword_color,

	"mat2x2": keyword_color,
	"mat2x3": keyword_color,
	"mat2x4": keyword_color,

	"mat3x2": keyword_color,
	"mat3x3": keyword_color,
	"mat3x4": keyword_color,

	"mat4x2": keyword_color,
	"mat4x3": keyword_color,
	"mat4x4": keyword_color,

	"vec2": keyword_color,
	"vec3": keyword_color,
	"vec4": keyword_color,
	"ivec2": keyword_color,
	"ivec3": keyword_color,
	"ivec4": keyword_color,
	"bvec2": keyword_color,
	"bvec3": keyword_color,
	"bvec4": keyword_color,

	"uint": keyword_color,
	"uvec2": keyword_color,
	"uvec3": keyword_color,
	"uvec4": keyword_color,

	"lowp": keyword_color,
	"mediump": keyword_color,
	"highp": keyword_color,
	"precision": keyword_color,

	"sampler1D": keyword_color,
	"sampler2D": keyword_color,
	"sampler3D": keyword_color,
	"samplerCube": keyword_color,

	"sampler1DShadow": keyword_color,
	"sampler2DShadow": keyword_color,
	"samplerCubeShadow": keyword_color,

	"sampler1DArray": keyword_color,
	"sampler2DArray": keyword_color,

	"sampler1DArrayShadow": keyword_color,
	"sampler2DArrayShadow": keyword_color,

	"isampler1D": keyword_color,
	"isampler2D": keyword_color,
	"isampler3D": keyword_color,
	"isamplerCube": keyword_color,

	"isampler1DArray": keyword_color,
	"isampler2DArray": keyword_color,

	"usampler1D": keyword_color,
	"usampler2D": keyword_color,
	"usampler3D": keyword_color,
	"usamplerCube": keyword_color,

	"usampler1DArray": keyword_color,
	"usampler2DArray": keyword_color,

	"sampler2DRect": keyword_color,
	"sampler2DRectShadow": keyword_color,
	"isampler2DRect": keyword_color,
	"usampler2DRect": keyword_color,

	"samplerBuffer": keyword_color,
	"isamplerBuffer": keyword_color,
	"usamplerBuffer": keyword_color,

	"struct": keyword_color,
}
