extends Control

var bands:Array[FrequencyBand] = []
func _ready():
	OS.open_midi_inputs()


func _process(delta: float):
	for band in bands:
		var amp := band.get_amplitude(delta)
		set_uniform(band.uniform_name,
				# not sure why this is 0.25 and not 1.0
				remap(amp, 0.0, 0.25,
				band.min, band.max))
	for pad in midi_pad_configs:
		pad.adsr.process(delta, pad.pressed)
		set_uniform(pad.uniform_name,
				remap(pad.adsr.value*lerp(1.0, pad.velocity, pad.velocity_contribution),
				0.0, 1.0, pad.min, pad.max))


var midi_cc_configs: Array[MidiCcConfig] = []
var midi_pad_configs: Array[MidiPadConfig] = []
var midi_key_configs: Array[MidiKeyConfig] = []
var midi_pressure_configs: Array[MidiPressureConfig] = []
func _input(e: InputEvent):
	if(not e is InputEventMIDI):
		return
	var event: InputEventMIDI = e
	if(event.message == MIDI_MESSAGE_CONTROL_CHANGE):
		for config in midi_cc_configs:
			if(config.cc == event.controller_number):
				set_uniform(config.uniform_name,
						remap(float(event.controller_value), 0.0, 127.0,
						config.min, config.max))
		return
	if(event.message == MIDI_MESSAGE_NOTE_ON and event.velocity == 0):
		# apparently some controllers expect this to be treated as a note off
		event.message = MIDI_MESSAGE_NOTE_OFF
	if(event.message == MIDI_MESSAGE_NOTE_ON):
		for config in midi_pad_configs:
			if(config.channel == event.channel and config.note == event.pitch):
				config.pressed = true
				config.velocity = float(event.velocity)/127.0
		for config in midi_key_configs:
			if(config.channel == event.channel and event.pitch >= config.min_note and event.pitch <= config.max_note):
				set_uniform(config.uniform_name,
						Vector2(float(event.pitch), float(event.velocity)/127.0))
	elif(event.message == MIDI_MESSAGE_NOTE_OFF):
		for config in midi_pad_configs:
			if(config.channel == event.channel and config.note == event.pitch):
				config.pressed = false
	elif(event.message == MIDI_MESSAGE_CHANNEL_PRESSURE):
		for config in midi_pressure_configs:
			if(config.channel == event.channel):
				set_uniform(config.uniform_name,
						remap(float(event.pressure), 0.0, 127.0,
						config.min, config.max))


func set_uniform(uniform_name: String, value):
	viz.material.set_shader_parameter(uniform_name, value)
	viz3d.get_surface_override_material(0).set_shader_parameter(uniform_name, value)


@onready var viz = %Viz
@onready var viz3d = %Viz3d
@onready var camera: Camera3D = %Camera3d
func _on_shader_edit_new_canvasitem_shader(s):
	while(not is_instance_valid(viz)):
		await get_tree().process_frame
	viz.material.set_shader(s)
	viz.visible = true
	camera.current = false


func _on_shader_edit_new_spatial_shader(s):
	while(not is_instance_valid(viz)):
		await get_tree().process_frame
	viz3d.get_surface_override_material(0).set_shader(s)
	viz.visible = false
	camera.current = true


@onready var world: SubViewportContainer = %World
func _on_shader_edit_new_resolution(r: float):
	while(not is_instance_valid(world)):
		await get_tree().process_frame
	world.stretch_shrink = 1.0/r


func _on_shader_edit_new_fft_size(s):
	AudioServer.get_bus_effect(0, 0).fft_size = s


func _on_shader_edit_new_band_config(c: FrequencyBand):
	bands.append(c)


func _on_shader_edit_new_cc_config(c: MidiCcConfig):
	midi_cc_configs.append(c)


func _on_shader_edit_new_key_config(c: MidiKeyConfig):
	midi_key_configs.append(c)


func _on_shader_edit_new_pad_config(c: MidiPadConfig):
	midi_pad_configs.append(c)


func _on_shader_edit_new_pressure_config(c: MidiPressureConfig):
	midi_pressure_configs.append(c)


func _on_shader_edit_reset_settings():
	bands = []
	midi_pressure_configs = []
	midi_pad_configs = []
	midi_key_configs = []
	midi_cc_configs = []
