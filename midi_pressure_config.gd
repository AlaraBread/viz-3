class_name MidiPressureConfig

var channel: int = 0
var uniform_name: String = ""
var min: float = 0.0
var max: float = 1.0
