class_name Adsr

var a: float = 0.0
var d: float = 0.1
var s: float = 0.6
var r: float = 0.05

enum AdsrState {
	WAITING,
	ATTACK,
	DECAY,
	SUSTAIN,
	RELEASE,
}
var state: AdsrState = AdsrState.WAITING
var value: float = 0.0

var prev_pressed: bool = false
func process(delta: float, pressed: bool):
	if(pressed and not prev_pressed):
		state = AdsrState.ATTACK
	elif(prev_pressed and not pressed):
		state = AdsrState.RELEASE
	match state:
		AdsrState.ATTACK:
			if(a == 0.0):
				state = AdsrState.DECAY
				value = 1.0
			else:
				value += delta/a
				if(value >= 1.0):
					state = AdsrState.DECAY
				value = clamp(value, 0.0, 1.0)
		AdsrState.DECAY:
			if(d == 0.0):
				state = AdsrState.SUSTAIN
				value = s
			else:
				value += delta*-(1.0-s)/d
				if(value <= s):
					state = AdsrState.SUSTAIN
				value = clamp(value, s, 1.0)
		AdsrState.SUSTAIN:
			value = s
		AdsrState.RELEASE:
			if(r == 0.0):
				state = AdsrState.WAITING
				value = 0.0
			else:
				value += delta*-s/r
				if(value <= 0.0):
					state = AdsrState.WAITING
				value = clamp(value, 0.0, 1.0)
	prev_pressed = pressed
