class_name MidiPadConfig

var channel: int = 1
var note: int = 60
var uniform_name: String = ""
var adsr: Adsr = Adsr.new()
var min := 0.0
var max := 1.0
var velocity_contribution: float = 0.5
var velocity: float = 1.0
var pressed: bool = false
