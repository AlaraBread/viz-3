class_name FrequencyBand

var uniform_name := ""
var min_freq := 50.0
var max_freq := 150.0
var min := 0.0
var max := 1.0

var bypass_damper := false

var f := 1000.0 :
	set(n):
		f = n
		damper.f = f
var z := 40.0 :
	set(n):
		z = n
		damper.z = z

var r := 2.0 :
	set(n):
		r = n
		damper.r = r

var damper: Damper
var analyzer: AudioEffectSpectrumAnalyzerInstance
func _init():
	damper = Damper.new()
	damper.f = f
	damper.z = z
	damper.r = r
	analyzer = AudioServer.get_bus_effect_instance(0, 0)

func get_amplitude(delta: float) -> float:
	var amp := analyzer.get_magnitude_for_frequency_range(min_freq, max_freq,
			AudioEffectSpectrumAnalyzerInstance.MAGNITUDE_MAX).length()
	if(bypass_damper):
		damper.update_damper(delta, 0.0)
		return amp
	return damper.update_damper(delta, amp)
