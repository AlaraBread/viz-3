class_name MidiKeyConfig

var channel: int = 0
var uniform_name: String = ""
var min_note: int = 0
var max_note: int = 127
